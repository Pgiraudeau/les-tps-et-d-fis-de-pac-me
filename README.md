# PROJET N°1 : LABYRINTH

Ce progamme vous permettera de créer votre propre labyrinthe de taille 11x11 puis de faire jouer vos amis à ces labyrinthes précédements créer.
2 labyrinthes sont fournis avec :
-Ok avec un niveau de difficulté OK
-Difficile avec un niveau plus difficile

## INSTALLER LE PROGRAMME

### Que sont les dossiers

- "rendu_graphique",  "éditeur",  "jouer" et "import_export" sont des modules
- "main" est le programme principale
- "laby_OK.txt" et "laby_super_diffcile.txt" sont des labyrinthes sous forme de document texte
- il y a aussi la video, celle-ci n'est pas importante pour le fonctionnement du programme

### Où l'installer ?

Dans un dossier quelquonque, l'important est que tous les fichiers soit au même endroit.

### Peut-on les renommer ?

Seul les fichiers "laby*.txt" on le droit d'être renommé ainsi que le programme "main". 
Les autres sont des modules et ne doivent en aucun cas subir une modification de nom. 
Si tout de même les modules voient leur nom changer, il faut aussi modifier la façon dont est importé le module dans "main".

## Lancer le programme

- Ouvrir "main" avec un IDE tel que visual studio
- Démarer le programme

## Le jeu

### Les trois options

#### Jouer

Vous permet de jouer à un labyrinth déjà enregistrer localement

- Choississez le fichier
- Appuyez sur les boutons de directions pour vous déplacer

#### Editeur

L'éditeur vous permettera de créer votre labyrinthe
il y a quatre actions possibles :
- bouger
- mettre ou enlever un mur
- enregistrer
- fermer

##### Bouger

Fait apparaître quatre cases de direction. En mode éditeur, les murs n'appliquent pas de contraintes

##### Mettre ou enlever un mur

Propose de mettre un mur si absence ou d'enlever un mur si présence dans la direction choisi

##### Enregistrer

Enregistre le fichier localement sous laby_NomChoisi.txt 
 !attention! Ce labyrinthe ne sera plus modifiable ( je viens d'y penser que maintenant que le contraire serait cool :/ )

##### Fermer

Ferme l'éditeur sans enregistrer le labyrinte 

#### Fermer

Ferme l'application


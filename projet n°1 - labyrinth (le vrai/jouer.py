def déplacement(coor:tuple, mouv:str)->tuple:
    '''
    coordonnée reçues : (x,y)
    renvoie les nouvelles coordonnées selon le mouvement mis en argument
    >>> déplacment( (5,3), 'gauche')
    (4,3)

    >>> déplacment( (5,3), 'droite')
    (6,3)

    >>> déplacment( (5,3), 'haut')
    (5,2)

    >>> déplacment( (5,3), 'bas')
    (5,4)
    '''

    # test du mouvement choisi
    if mouv == "gauche":
        # changement de coordonné
        new_coor = (coor[0]-1, coor[1])

    # test du mouvement choisi
    elif mouv == "droite":
        # changement de coordonné
        new_coor = (coor[0]+1, coor[1])

    # test du mouvement choisi
    elif mouv == "haut":
        # changement de coordonné
        new_coor = (coor[0], coor[1]-1)

    # test du mouvement choisi
    elif mouv == "bas":
        # changement de coordonné
        new_coor = (coor[0], coor[1]+1)
    
    # si mauvais boutons appuyé
    elif mouv == '        ':
        new_coor = (coor[0], coor[1])

    # renvoie les nouvelles coordonnées
    return new_coor



def dep_possible(coor:tuple, tableau:list)->list:
    '''
    renvoie la liste des déplacements possibles
    '''

    # création de la liste qui contiendra les differents déplacements possibles
    dep_pos = ['        ','        ','        ','        ']

    ## tests effectués pour chaque mouvement possible : gauche, droite, bas et haut
    # 1er test : test des bords du tableau
    #     2eme test : test de la présence d'un mur
    #         ajout de la direction dans la liste

    if coor[0] > 0:
        if not(tableau[2*coor[1]][coor[0]-1]):
            dep_pos[0] = 'gauche'

    if coor[0] < 10: 
        if not(tableau[2*coor[1]][coor[0]]):
            dep_pos[1] = 'droite'
    
    if coor[1] < 10:
        if not(tableau[2*coor[1]+1][coor[0]]):
            dep_pos[2] = 'bas'

    if coor[1] > 0:
        if not(tableau[2*coor[1]-1][coor[0]]):
            dep_pos[3] = 'haut'
    
    # renvoie de la liste contenant les possibles mouvements
    return dep_pos



def importation(nom_fichier:str)->list:
    '''
    ouvre le fichier du nom rentré et le lit pour le transformer en liste de liste de bouléen
    str => list[list[bool]]
    '''
    # ouvre le fichier
    lecteur = open(nom_fichier, 'r')

    # le transforme en str
    texte = lecteur.read()

    # le fragmente en une liste
    tableau_intermédiaire = texte.split('\n')

    ## fragmentation des lignes et remplissage du tableau, tableau = list[list[str]]
    tableau = []
    for ligne in tableau_intermédiaire:
        # fragmentation de la ligne
        ligne_tab = ligne.split(';')

        # déletion du dernière élément qui était une chaîne de caractère vide
        del ligne_tab[len(ligne_tab)-1]

        # la ligne est rentrée dans le tableau
        tableau.append(ligne_tab)

    # transformation de chaque case du tableau en booléen: str -> bool
    for ligne in range(len(tableau)):
        for case in range(len(tableau[ligne])):
            tableau[ligne][case] = eval(tableau[ligne][case])

    # déletion de la denière ligne du tableau qui était une ligne vide
    del tableau[len(tableau)-1]

    # renvoie du tableau
    return tableau



def exportation(nom:str, tableau)->None:
    '''
    créer un fichier txt du nom choisi et le tableau y sera écrit:
    séparation des lignes du tableau : '\n'
    séparation des colonnes du tableau : ';'
    '''

    # création du tableau qui servira de reeptacle
    tableau_ecrit = ''

    for ligne in tableau:

        #création d'une chaîne de caractère qui accueillera la ligne sous forme de str
        ecrit = ''

        # pour chaque colonne, transformation du booléen en chaîne de caractère et ajout de ';' pour séparer deux colonnes
        for colonne in ligne:
            ecrit += str(colonne) + ';'

        # ajout de '\n' pour séparer deux ligne
        tableau_ecrit += ecrit + '\n'

    ## création du fichier du nom de laby_nom.txt et écriture du tableau
    with open('laby_' + nom + '.txt', 'w') as fic_laby:
            fic_laby.write(tableau_ecrit)

    # ne renvoie rien
    return None


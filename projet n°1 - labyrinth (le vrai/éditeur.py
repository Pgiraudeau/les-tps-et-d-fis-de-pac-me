### déplacements

def déplacement(coor:tuple, mouv:str)->tuple:
    '''
    coordonnée reçues : (x,y)
    renvoie les nouvelles coordonnées selon le mouvement mis en argument
    >>> déplacment( (5,3), 'gauche')
    (4,3)

    >>> déplacment( (5,3), 'droite')
    (6,3)

    >>> déplacment( (5,3), 'haut')
    (5,2)

    >>> déplacment( (5,3), 'bas')
    (5,4)
    '''

    # test du mouvement choisi
    if mouv == "gauche":
        # changement de coordonné
        new_coor = (coor[0]-1, coor[1])

    # test du mouvement choisi
    elif mouv == "droite":
        # changement de coordonné
        new_coor = (coor[0]+1, coor[1])

    # test du mouvement choisi
    elif mouv == " haut ":
        # changement de coordonné
        new_coor = (coor[0], coor[1]-1)

    # test du mouvement choisi
    elif mouv == "  bas  ":
        # changement de coordonné
        new_coor = (coor[0], coor[1]+1)
    
    # si mauvais bouton appuyé
    elif mouv == '        ':
        new_coor = (coor[0], coor[1])

    # renvoie les nouvelles coordonnées
    return new_coor



def dep_possible(coor:tuple)->list:
    '''
    renvoie la liste des déplacements possibles
    '''

    # liste qui contiendra les choix possibles de déplacement
    dep_pos = ['        ','        ','        ','        ']

    ### test pour les différentes directions : gauche, droite, bas et haut
    # test des bords du tableau
    #     ajout de la possibilité si condition remplie
    # (pas de test sur les murs car on les traverses en mode éditeur)

    if coor[0] > 0:
        dep_pos[0] = 'gauche'

    if coor[0] < 10:
        dep_pos[1] = 'droite'
    
    if coor[1] < 10:
        dep_pos[2] = '  bas  '

    if coor[1] > 0:
        dep_pos[3] = ' haut '

    # renvoie de la liste contenant les possibilités 
    return dep_pos


### Travail sur les murs

def murs_possible(coor:tuple)->list:
    '''
    renvoie la liste des murs modifiables
    '''
    dep_pos = ['        ','        ','        ','        ']

    ### test pour les différentes directions : gauche, droite, bas et haut
    # test des bords du tableau
    #     ajout de la possibilité si condition remplie

    if coor[0] > 0:
        dep_pos[0] = 'gauche'

    if coor[0] < 10:
        dep_pos[1] = 'droite'
    
    if coor[1] < 10:
        dep_pos[2] = '  bas  '

    if coor[1] > 0:
        dep_pos[3] = ' haut '

    # renvoie de la liste des possibilités
    return dep_pos


def murs(tableau, coor:tuple, direction:str):
    '''
    enlève le mur dans la direction définie si il y a un mur
    met un mur dans la direction définie si il n'y a pas de mur

    -----------------------------------

    +---+---+---+
    |   |   |   |
    +---+---+---+
    |   | 0 |   |  (après affichage)
    +---+---+---+
    |   |   |   |
    +---+---+---+
          |          >>>    murs(tableau, (1,1), 'bas')
          V
    +---+---+---+
    |   |   |   |
    +---+---+---+
    |   | 0 |   |  (après affichage)
    +---+   +---+
    |   |   |   |
    +---+---+---+

    -----------------------------------

    +---+---+---+
    |           |
    +   +   +   +
    |     0     |  (après affichage)
    +   +   +   +
    |           |
    +---+---+---+
          |          >>>    murs(tableau, (1,1), 'bas')
          V
    +---+---+---+
    |           |
    +   +   +   +
    |     0     |  (après affichage)
    +   +---+   +
    |           |
    +---+---+---+
    '''

    ## test de la direction choisie
    ##     changement des coordonnées du mur 
    ##     pour un mur : tableau[ordonné][absicisse]

    if direction == "gauche":
        # tableau[2*coor[1]][coor[0]-1] => coordonnées du mur gauche
        tableau[2*coor[1]][coor[0]-1] = not(tableau[2*coor[1]][coor[0]-1])

    if direction == "droite":
        # tableau[2*coor[1]][coor[0]] => coordonnées du mur gauche
        tableau[2*coor[1]][coor[0]] = not(tableau[2*coor[1]][coor[0]])

    if direction == "bas":
        # tableau[2*coor[1]+1][coor[0]] => coordonnées du mur gauche
        tableau[2*coor[1]+1][coor[0]] = not(tableau[2*coor[1]+1][coor[0]])

    if direction == "haut":
        # tableau[2*coor[1]-1][coor[0]] => coordonnées du mur gauche
        tableau[2*coor[1]-1][coor[0]] = not(tableau[2*coor[1]-1][coor[0]])

    # renvoie du nouveau tableau/labyrinth
    return tableau

### IMPORTATION DES MODULES 

# interface graphique : module easygui
import easygui as eg

# modules créés localement
import rendu_graphique as graph
import éditeur as ed
import import_export as imp_ex
import jouer


titre = 'LABYRINTH'


activé = True
# boucle continue jusqu'à ce que False soit affecté à activé 
while activé:

    ## création des paramètres par défault : tableau et coordonnées

    # remplissage du tableau entièrement de True
    tableau =  []
    for ligne in range(1,22):
        
         ligne_creation = []
         if ligne%2 == 0:
            for colonne in range(11):
                ligne_creation.append(True)
         else:
            for colonne in range(10):
                ligne_creation.append(True)
         tableau.append(ligne_creation)
    
    # coordonnée au départ
    mes_coor = (5,0)

    ### demande de ce que l'utilisateur souhaite faire

    # choix possibles
    choix = ['éditeur', 'Jouer', 'fermer']

    # saisie de l'utilisateur via easygui
    identité = eg.buttonbox(msg='choisissez', title=titre, choices=choix)

    if identité == "Jouer":
        # activation du mode éditeur et désactivation du mode joueur
        jouer_activé = True
        editeur_activé = False

    if identité == "éditeur":
        # activation du mode joueur et désactivation du mode éditeur
        editeur_activé = True
        jouer_activé = False

    if identité == "fermer":
        # ferme le programme
        activé = False

    ### actions du mode editeur
    while editeur_activé:

        # choix possibles du mode éditeur
        choix = ['Bouger', 'Mettre ou enlever un mur', 'Enregistrer', 'Fermer']
        # choix de l'utilisateur
        action = eg.buttonbox(msg=graph.rendu_graphique(tableau,mes_coor)+'\n Quelle action ?', title=titre, choices=choix)

    
        ## instruction lorsque 'Bouger' est choisie
        if action == "Bouger":
            # demande du mouvement à faire
            mouvement = eg.buttonbox(msg=graph.rendu_graphique(tableau,mes_coor), title=titre, choices=ed.dep_possible(mes_coor))
            # changement des coordonnées
            mes_coor = ed.déplacement(mes_coor, mouvement)
        
        ## instruction lorsque 'Mettre ou enlever un mur' est choisie
        elif action == 'Mettre ou enlever un mur':
            # demande du murs à mettre ou à enlever
            mur_choisi = eg.buttonbox(msg=graph.rendu_graphique(tableau,mes_coor), title=titre, choices=ed.murs_possible(mes_coor))
            # modifiction du tableau/labyrinth en conséquence
            tableau = ed.murs(tableau, mes_coor, mur_choisi)
        
        ## instruction lorsque 'Fermer' est choisie
        elif action == 'Fermer':
            # fermeture du mode éditeur
            editeur_activé = False
        
        ## instruction lorsque 'Enregistrer' est choisie
        elif action == 'Enregistrer':
            # demande du nom sous lequel le labyrinth sera enregistrer
            nom = eg.enterbox(msg=graph.rendu_graphique(tableau,mes_coor) + 'Quel nom voulez-vous lui donner ?', title=titre)
            # enregistrement
            imp_ex.exportation(nom, tableau)
            # fermeture du mode éditeur
            éditeur_activé = False

    ### actions du mode Joueur
    while jouer_activé:

        # choix du fichier/labyrinth, il sera sous forme de chemin absolue
        chemin_tableau = eg.fileopenbox(msg='choisissez votre fichier laby', title=titre, filetypes='*.txt')
        # fragmention du chemin absolue
        chemin_séparé = chemin_tableau.split("\u005C")
        # sélection du dernier fragment
        nom_tableau = chemin_séparé[-1]
        # importation du tableau/labyrinth
        tableau_choisi = imp_ex.importation(str(nom_tableau))



        non_victoire = True
        ## début du jeu 
        while non_victoire:
            # demande du mouvement
            dep = eg.buttonbox(msg=graph.rendu_graphique(tableau_choisi, mes_coor), title=titre, choices=jouer.dep_possible(mes_coor, tableau_choisi))
            # changement des coordonnées
            mes_coor = jouer.déplacement(mes_coor, dep)
            # conditon de victoire
            if mes_coor == (5,10):
                non_victoire = False

        # message de victoire
        eg.msgbox(msg='VICTOIRE', title=titre)
        jouer_activé = False
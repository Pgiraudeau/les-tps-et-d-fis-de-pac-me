

def rendu_graphique(tableau:list[list[bool]], coor:tuple)->list[list[bool]]:
    '''
    transforme le tableau/labyrinth en chaîne de caractère et place '0' au coordonnées données
    
    >>> tableau = [
                   [False, True, False]
                   [False, False, False, True]
                   [True, False, False]
                   [False, True, True, False]
                   [True, False, True]
                   [False, False, True, False]
                   [True, False, False]
                  [
    >>> rendu_graphique(tableau, (0,2)

    +---+---+---+---+
    |       |       |
    +   +   +   +---+
    |   |           |
    +   +---+---+   +
    | 0 |       |   |
    +   +   +---+   +
    |   |           |
    +---+---+---+---+

    '''
    # haut du labyrinth (inchangeable)
    msg = '+---+---+---+---+---+   +---+---+---+---+---+\n'

    ## ce comptage des lignes servira à placer le pion grâce aux coordonnées et à savoir quel sorte de mur il sagit (horizontal ou vertical)
    # une ligne ayant une valeur entière est vertical, les autres sont horizontal
    num_ligne = 0

    # pour chaque ligne du tableau
    for ligne in tableau:

        ### debut de chaque ligne (inchangeable)

        # reste de num_ligne divisé par 1 = 0  =>  vertical
        if num_ligne % 1 == 0:
            msg += '|'
        # reste de num_ligne divisé par 1 != 0  =>  horizontal
        else:
            msg += '-'

        # comptage des colonnes pour placer le pion
        num_col = 0
        for col in ligne:

            # correspond aux coordonnés
            if (num_col, num_ligne) == coor:

                # test de la présence d'un mur
                if col:
                    msg += ' 0 |'
                else:
                    msg += ' 0  '

            # reste de num_ligne divisé par 1 = 0  =>  vertical
            elif num_ligne%1 == 0:

                # test de la présence d'un mur
                if col:
                    msg += '   |'
                else:
                    msg += '    '

            # reste de num_ligne divisé par 1 != 0  =>  horizontal
            else:

                # test de la présence d'un mur
                if col:
                    msg += '---+'
                else:
                    msg += '   +'

            # incrémentation de num_col
            num_col +=1

        ## dernier mur (inchangeable)

        # correspond aux coordonnés
        if (num_col, num_ligne) == coor:
            msg += ' 0 |'

        # reste de num_ligne divisé par 1 = 0  =>  vertical  =>  '   |'
        elif num_ligne%1 == 0:
            msg += '   |'

        # reste de num_ligne divisé par 1 != 0  =>  horizontal  =>  ''
        else:
            msg += ''

        # ligne du tableau suivante  =>  saut de ligne
        msg += '\n'

        # incrément nécessaire
        num_ligne += 0.5

    # bas du labyrinth (inchangeable)s
    msg += '+---+---+---+---+---+   +---+---+---+---+---+\n'
    msg += '                    arrivé'

    # renvoie la chaîne de caractère, tout beau tout propre
    return msg



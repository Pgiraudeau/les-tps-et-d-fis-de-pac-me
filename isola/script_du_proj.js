alert('yolo');

function arraysIdentical(a, b) {
    if (a[0] == b[0] && a[1] == b[1])
    {
        return true;
    }
    else{
        return false;
    }
    
};


function case_detruite(plateau_isola, case_cliquee){
    console.log(plateau_isola[case_cliquee[0]][case_cliquee[1]])
    if (plateau_isola[case_cliquee[0]][case_cliquee[1]] == 0)
    {
        plateau_isola[case_cliquee[0]][case_cliquee[1]] = -1;
        if (etape == 2)
        {
            etape += 1
        }
        else
        {
            etape = 1
        }
    }
    else 
    {
        alert("Veuillez resélectionnez une case celle-ci n'est pas dispo.");
    }

}


var coor = [0,0];
var plateau = 
[
    [-1,-1,-1,-1,-1,-1,-1],
    [-1,0,0,0,1,0,0,0,-1],
    [-1,0,0,0,0,0,0,0,-1],
    [-1,0,0,0,0,0,0,0,-1],
    [-1,0,0,0,0,0,0,0,-1],
    [-1,0,0,0,0,0,0,0,-1],
    [-1,0,0,0,0,0,0,0,-1],
    [-1,0,0,0,2,0,0,0,-1],
    [-1,-1,-1,-1,-1,-1,-1],
];

var case_cliquee = [-3,-3];


function test_de_la_mort(id)
{
    case_cliquee = [id[2], id[0]];
    etape_suivante();
}


var num_to_classe = new Map();
num_to_classe.set(-1, 'lave');
num_to_classe.set(0, 'herbe');
num_to_classe.set(1, 'J1');
num_to_classe.set(2, 'J2');


function actualisation(plateau)
{
    for (var ligne in plateau)
    {
        for (var colonne in plateau[ligne]) 
        {
            document.getElementById(colonne+"/"+ligne).className = num_to_classe.get(plateau[ligne][colonne]);
            
        }
    }
}


actualisation(plateau);


function deplacement_j1 (coor_joueurs_1, cases_dispos, plateau_isola) 
{
    var case_dans_dispo_ = false;
    for (i in cases_dispos)
    {
        case_dans_dispo_ = case_dans_dispo_ || arraysIdentical(case_cliquee, cases_dispos[i]);
    }

    if (case_dans_dispo_)
    {
        plateau_isola[case_cliquee[0]][case_cliquee[1]] = 1;
        plateau_isola[coor_joueurs_1[0]][coor_joueurs_1[1]] = 0;

        return plateau_isola
    }
    else
    {
        alert("La case est invalide")
        etape -= 1
        return plateau_isola
    }
}

function deplacement_j2 (coor_joueurs_2, cases_dispos, plateau_isola) 
{
    
    var case_dans_dispo_ = false;
    for (i in cases_dispos)
    {
        case_dans_dispo_ = case_dans_dispo_ || arraysIdentical(case_cliquee, cases_dispos[i]);
    }

    if (case_dans_dispo_)
    {
        plateau_isola[case_cliquee[0]][case_cliquee[1]] = 2;
        plateau_isola[coor_joueurs_2[0]][coor_joueurs_2[1]] = 0;

        return plateau_isola
    }
    else
    {
        alert("La case est invalide")
        etape += 1
        return plateau_isola
    }

}

function cases_dispos_J2(plateau)
{
    var y = 0;
    var x = 0;
    while (plateau[y][x] != 2)
    {
        y = y + 1;
        x = 0;

        for (var i = 1; i < 9 && plateau[y][x] != 2; i++)
        {
            x = x + 1;
        }
        
    }
    var cases_possibles = 
    [
        [y+1, x], 
        [y+1, x+1], 
        [y, x+1], 
        [y-1, x+1], 
        [y-1, x], 
        [y-1, x-1], 
        [y, x-1], 
        [y+1, x-1]
    ];
    
    var cases_dispos = [];
    for (var case_testee in cases_possibles)
    {
        if (plateau[cases_possibles[case_testee][0]][cases_possibles[case_testee][1]] == 0)
        {
            cases_dispos.push(cases_possibles[case_testee]);
        }
    }
    coor = [y, x];

    if (cases_dispos.length == 0)
    {
        alert("victoire J1")
        plateau = 
        [
            [-1,-1,-1,-1,-1,-1,-1],
            [-1,0,0,0,1,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,2,0,0,0,-1],
            [-1,-1,-1,-1,-1,-1,-1],
        ];
        actualisation()
        etape = 0

    }
    return cases_dispos;
    
}




function cases_dispos_J1(plateau)
{
    var y = 0;
    var x = 0;
    while (plateau[y][x] != 1)
    {
        y = y + 1;
        x = 0;

        for (var i = 1; i < 9 && plateau[y][x] != 1; i++)
        {
            x = x + 1;
        }
        
    }
    var cases_possibles = 
    [
        [y+1, x], 
        [y+1, x+1], 
        [y, x+1], 
        [y-1, x+1], 
        [y-1, x], 
        [y-1, x-1], 
        [y, x-1], 
        [y+1, x-1]
    ];
    
    var cases_dispos = [];
    for (var case_testee in cases_possibles)
    {
        if (plateau[cases_possibles[case_testee][0]][cases_possibles[case_testee][1]] == 0)
        {
            cases_dispos.push(cases_possibles[case_testee]);
        }
    }

    if (cases_dispos.length == 0)
    {
        alert("victoire J2")
        plateau = 
        [
            [-1,-1,-1,-1,-1,-1,-1],
            [-1,0,0,0,1,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,0,0,0,0,-1],
            [-1,0,0,0,2,0,0,0,-1],
            [-1,-1,-1,-1,-1,-1,-1],
        ];
        actualisation()
        etape = 1

    }
    coor = [y, x];
    return cases_dispos
    
}


var cases_dispos = cases_dispos_J1(plateau);
alert(cases_dispos + "  -  " + coor)

console.log("J1")
etape = 1
function etape_suivante()
{
    if (etape == 1)
    {
        
        
        plateau = deplacement_j1(coor, cases_dispos, plateau )
        actualisation(plateau)
        etape += 1
        console.log(etape)

    }

    else if (etape == 2)
    {
        case_detruite(plateau, case_cliquee)
        actualisation(plateau)
        cases_dispos = cases_dispos_J2(plateau);
    } 

    else if (etape == 3) 
    {
        
        
        plateau = deplacement_j2(coor, cases_dispos, plateau)
        actualisation(plateau)
        etape += 1
        console.log(etape)
    } 

    else 
    {
        case_detruite(plateau, case_cliquee)
        actualisation(plateau)
        cases_dispos = cases_dispos_J1(plateau);
    }
    actualisation()
}

actualisation()

import pygame
import random as rd 
import math 
import numpy as np
import easygui as eg 

titre = 'Simulation pandémie'
msg = "nombre d'individus saint :"
défaut = '25'
nb_individus_saits = int(eg.enterbox(msg, titre, défaut))
msg = "nombre d'individus malades :"
défaut = '5'
nb_individus_malades = int(eg.enterbox(msg, titre, défaut))
msg = "taille individu :"
défaut = '5'
taille_individus = int(eg.enterbox(msg, titre, défaut))
msg = "temps avant d'être soigné (en sec) :"
défaut = '5'
tps_soignement = int(eg.enterbox(msg, titre, défaut))

WHITE =  (255, 255, 255) 
BLUE =  ( 0, 0, 255) 
GREEN =  ( 0, 255, 0) 
RED =  (255, 0, 0) 
TEXTCOLOR = ( 0, 0, 0) 
(width, height) = (720, 480)
background_color = WHITE
FPS = 60
clock = pygame.time.Clock()

running = True

class point():
    x = 0
    y = 0
    vx = 0
    vy = 0
    r = taille_individus
    couleur = (0, 255,0)
    time_contaminé = 0

    def avancer(self):
        self.x += self.vx
        self.y += self.vy
        return self.x,self.y

    def contaminé(self):
        color = self.couleur
        # self.couleur = (255-color[0], 255-color[1], 0)
        self.couleur = (255, 0 , 0)
        return self.couleur

    def soigné(self):
        self.couleur = (0, 0, 255)
        return self.couleur

    def rebond(self):

        if self.x + self.vx > 720 or self.x + self.vx < 0:
            self.vx = -self.vx

        if self.y + self.vy > 480 or self.y + self.vy < 0:
            self.vy = -self.vy

        return self.vx,self.vy


def choc(b1, b2):
    x1, x2 , y1, y2 = b1.x, b2.x, b1.y, b2.y
    vx1, vx2, vy1, vy2 = b1.vx, b2.vx, b1.vy, b2.vy
    dx = x2 - x1
    dy = y2 - y1
    dist = math.sqrt(dx**2 + dy**2)

    if dist <= b2.r + b1.r +0.1 and not(dist == 0):
        nx, ny = dx/dist, dy/dist # vecteur normal unitaire 
        tx , ty = -ny, nx # vecteur tangente unitaire

        vn1 = (vx1*nx + vy1*ny) # projection 1 normale
        vt1 = (vx1*tx + vy1*ty) # projection 1 tangentielle 
        vn2 = (vx2*nx + vy2*ny) # projection 2 normale
        vt2 = (vx2*tx + vy2*ty) # projection 2 tangentielle

        vnP1, vnP2 = vn2, vn1 # les vecteurs normaux sont échangés comme un choc 1D
        vtP1, vtP2 = vt1, vt2 # vecteurs tan inchangés

        b1.vx = vnP1*nx + vt1*tx # v = vn + vt
        b1.vy = vnP1*ny + vt1*ty
        b2.vx = vnP2*nx + vt2*tx
        b2.vy = vnP2*ny + vt2*ty

        if (b1.couleur == (255,0,0) or b2.couleur == (255,0,0)) and not(b1.couleur == (0,0,255) or b2.couleur == (0,0,255)):
            b1.contaminé()
            b2.contaminé()


def test_choc(tab, b):
    for k in tab:
        if k != b:
            choc(k,b)


def main():
    pygame.init()
    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption('RONDS ! Ce sont des ronds !')
    screen.fill(background_color)
    pygame.display.update()
    
    l =[]

    for i in range(nb_individus_malades):
        p_tempo = point() 
        p_tempo.x, p_tempo.y, p_tempo.vx, p_tempo.vy, p_tempo.couleur = rd.randint(20, 700), rd.randint(20, 460), rd.randint(-2,2), rd.randint(-2,2), (255,0,0)
        l.append(p_tempo)
    
    for i in range(nb_individus_saits):
        p_tempo = point() 
        p_tempo.x, p_tempo.y, p_tempo.vx, p_tempo.vy = rd.randint(20, 700), rd.randint(20, 460), rd.randint(-2,2), rd.randint(-2,2)
        
        l.append(p_tempo)  
    running = True

    while running:

        clock.tick(FPS)
        screen.fill(background_color)

        ev = pygame.event.get()

        for event in ev:
            if event.type == pygame.QUIT: 
                running = False 


        for cercle in l:
            if cercle.couleur == (255, 0, 0):
                if cercle.time_contaminé > tps_soignement*60:
                    cercle.soigné()
                else:
                    cercle.time_contaminé += 1
            cercle.avancer()
            cercle.rebond()
            test_choc(l, cercle)
            pygame.draw.circle(screen, cercle.couleur, (int(cercle.x), int(cercle.y)), int(cercle.r)) 

        pygame.display.update()


if __name__ == '__main__': 
    main() 

pygame.quit()
def survie_en_fonction_de_la_classe(liste):

    survie = {1:0, 2:0, 3:0}
    for passager in liste:
        survie[int(passager['classe'])] += int(passager['survie'])

    nb_classe = {1:0, 2:0, 3:0}
    for passager in liste:
        nb_classe[int(passager['classe'])] += 1


    survie_pourcent_classe = {}
    for i in range(3):
        survie_pourcent_classe[i+1] = survie[i+1]/nb_classe[i+1]*100

    x = []
    y = []
    for i in range(len(survie_pourcent_classe)):
        x.append(min(survie_pourcent_classe))
        y.append(survie_pourcent_classe[min(survie_pourcent_classe)])
        del survie_pourcent_classe[min(survie_pourcent_classe)]

    plt.plot(x, y, c='g', ls='', marker='o', )
    plt.show()
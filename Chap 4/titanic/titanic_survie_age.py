def survie_en_fonction_de_l_age(liste):

    age_survie = {}
    nb_age = {}

    for passager in liste:
        if int(passager['age']) in nb_age:
            nb_age[int(passager['age'])] += 1
        else:
            nb_age[int(passager['age'])] = 1
            age_survie[int(passager['age'])] = 0

    for passager in liste:
        age_survie[int(passager['age'])] += int(passager['survie'])




    survie_pourcent_age = {}

    for i in nb_age.keys():
        survie_pourcent_age[i] = int(age_survie[i]/nb_age[i]*100)



    x = []
    y = []
    for i in range(len(survie_pourcent_age)):
        x.append(min(survie_pourcent_age))
        y.append(survie_pourcent_age[min(survie_pourcent_age)])
        del survie_pourcent_age[min(survie_pourcent_age)]

    plt.plot(x, y, c='g', ls='-', marker='o', )
    plt.show()
BaseAliens = [
 {'NomAlien': 'Zorglub', 'Sexe': 'M', 'Planete': 'Trantor', 'NoCabine': 1},
 {'NomAlien': 'Blorx', 'Sexe': 'M', 'Planete': 'Euterpe', 'NoCabine': 2},
 {'NomAlien': 'Urxiz', 'Sexe': 'M', 'Planete': 'Aurora', 'NoCabine': 3},
 {'NomAlien': 'Zbleurdite', 'Sexe': 'F', 'Planete': 'Trantor', 'NoCabine': 4},
 {'NomAlien': 'Darneurane', 'Sexe': 'M', 'Planete': 'Trantor', 'NoCabine': 5},
 {'NomAlien': 'Mulzo', 'Sexe': 'M', 'Planete': 'Helicon', 'NoCabine': 6},
 {'NomAlien': 'Zzzzzz', 'Sexe': 'F', 'Planete': 'Aurora', 'NoCabine': 7},
 {'NomAlien': 'Arghh', 'Sexe': 'M', 'Planete': 'Nexon', 'NoCabine': 8},
 {'NomAlien': 'Joranum', 'Sexe': 'F', 'Planete': 'Euterpe', 'NoCabine': 9}
]


def rentrer_un_alien(liste:list, nom:str, sexe:str, planete:str, num_cab:str):
    liste.append({'NomAlien': nom, 'Sexe': sexe, 'Planete': planete, 'NoCabine': num_cab})
    return liste

def pourcent_planete(liste, planete):
    '''
    donne le pourcentage d'aliens provenants de la planète rentrée
    '''
    nb=0
    for ligne in liste:
        if ligne['Planete'] == planete:
            nb += 1
    return int(nb/len(liste)*100)

def genre(liste, genre):
    '''
    donne tous les occupents ayant le sexe entré
    '''
    liste_genre = []
    for ligne in liste:
        if ligne['Sexe'] == genre:
            liste_genre.append(ligne)
    return liste_genre


def info_cab_to_cab(liste, d:int, f:int):
    '''
    donne les occupents des cabines de d à f compris
    '''
    liste_nouv = []
    for ligne in range(d, f+1):
        liste_nouv.append(ligne)
    return liste_nouv
def somme(n):
    if n == 0:
        return 0
    else:
        return somme(n-1) + n


def somme_carre(n):
    if n == 0:
        return 0
    else:
        return somme_carre(n-1) + (n**2)
        

def u(n):
    if n == 0:
        return 2
    else:
        return 4*u(n-1)+5


def fibo(a, b, n):
    if n == 0:
        return a
    elif n == 1:
        return b
    else:
        return fibo(a, b, n-1) + fibo(a, b, n-2)


def PGCD(a,b):
    if not a%b:
        return b
    else:
        return (PGCD(b, a%b))


def factoriel(n):
    if n == 0:
        return 1
    else:
        return factoriel(n-1) * n


print(PGCD(66,18))
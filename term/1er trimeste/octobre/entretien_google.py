from piles_files_list import Piles

def verif(chaine: str) -> bool:
    '''
    >>> verif("([{}])")
    True

    >>> verif("[(])")
    False

    >>> verif("(([]{})")
    False 

    >>> verif("([]{}))")
    False 

    >>> verif("(([]{()})){[]}")
    True

    >>> verif("[qbq(sdn)251453{jg}([ui][yu])]")
    True
    '''
    test = {"{":'}', "(":")", "[":"]"}
    p = Piles()

    for cara in chaine:

        if cara in test.keys():
            p.empiler(cara)

        elif cara in test.values():
            if p.est_vide():
                return False
            if test[p.depiler()] != cara:
                return False

    return p.est_vide()



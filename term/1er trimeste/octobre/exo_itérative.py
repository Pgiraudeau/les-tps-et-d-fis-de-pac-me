def somme(n):
    S = 0
    for i in range(1,n+1):
        S += i
    return S


def somme_carre(n):
    S = 0
    for i in range(1,n+1):
        S += i**2
    return S


def fibo(a,b,n):
    for i in range(n):
        print(i+1, " : ", a, " + ", b, " = ", a+b)
        a, b = b, a + b
        
    return b


def PGCD(a,b):
    while a%b:
        a, b = b, a%b
    return b



print(PGCD(77,21))
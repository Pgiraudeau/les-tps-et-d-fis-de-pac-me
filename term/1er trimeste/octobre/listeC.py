from __future__ import annotations
from typing import Generic, TypeVar, Optional
import random

T = TypeVar("T")

class Maillon(Generic[T]):

    def __init__(self: Maillon[T], val: T) -> None:
        self._val = val
        self._suiv: Optional[Maillon[T]] = None

    def get_val(self: Maillon[T]) -> T:
        return self._val 

    def get_suiv(self: Maillon[T]) -> Optional[Maillon[T]]:
        return self._suiv

    def set_suiv(self: Maillon[T], m: Optional[Maillon[T]]) -> None:
        if m is None or (type(m._val) == type(self._val)):
            self._suiv = m
        else:
            raise TypeError  

    def __repr__(self: Maillon[T]) -> str:
        return f"[{self._val}]-->{None if self._suiv is None else self._suiv._val}"


class ListeC(Generic[T]):

    def __init__(self: ListeC[T]) -> None:
        self._tete: Optional[Maillon[T]] = None

    def est_vide(self: ListeC[T]) -> bool:
        return self._tete is None

    def queue(self: ListeC[T]) -> ListeC[T]:
        qt: ListeC[T] = ListeC()
        if not self._tete is None:
            qt._tete = self._tete.get_suiv() 
        return qt

    def insere_tete(self: ListeC[T], val: T) -> None:
        if self._tete is None:
            self._tete = Maillon(val)
        else:
            t = Maillon(val)
            t.set_suiv(self._tete)
            self._tete = t
            
    def __repr__(self: ListeC[T]) -> str:
        if self.est_vide():
            return "Vide"
        if self.queue().est_vide():
            return f"Tete : {self._tete} --> Queue : Vide"
        return f"Tete : {self._tete.get_val()} --> Queue : {self.queue()._tete.get_val()} --> ?" 

    def __len__(self: ListeC[T]) -> int:
        if self.est_vide():
            print("Vide")
            return 0
        else:
            print(f"{self._tete}")
            return 1 + self.queue().__len__()


    def insere_fin(self: ListeC, val: T) -> None:
        if self._tete is None:
            self.insere_tete(val)
        if self.queue().est_vide():
            q = Maillon(val)
            self._tete.set_suiv(q)
        else:
            self.queue().insere_fin(val)
    
    def get_el(self: ListeC, indice: int) -> T:
        if indice == 0:
            print(self._tete._val)
            return self._tete._val
        else:
            self.queue().get_el(indice-1)

    def insere_entre(self: ListeC, indice: int, val: T) -> None:
        if indice == 0:
            m = Maillon(val)
            self._tete._suiv, m._suiv = m.suiv, self.queue()
            return None
        else:
            print(indice)
            self.queue().get_el(indice-1)
    
    def map(self: ListeC, fonction: str):
        copie = ListeC()
        for i in range(len(self)-1):
            m = eval(fonction + "( self.get_el(i) )")
            copie.insere_fin(m)
        return copie


def carre(x):
    return x**2


ls = ListeC()
print(ls.est_vide())
for nb in random.choices(range(1000), k = 10):
    ls.insere_tete(nb)

len(ls)
ls.map("carre")
len(ls)
print(ls.get_el(2))
import turtle as tut 
import random as rd



### Constantes

x = 7 #nb d'immeuble
étage_min = 5
étage_max = 6
tut.colormode(255)
tut.penup()
tut.speed(0) # vitesse de construction 
#                   1 peu rapide 
#                   |
#                   V
#                   10 très rapide 
#                   0 = vitesse la plus rapide

### construction tut

def trait(n:int)->None:
    '''
    fait un trait de 'n' longueur
    '''
    tut.pendown()
    tut.fd(n)
    tut.penup()

def rectangle(l:int, L:int)->None:
    '''
    fait un rectangle de 'l' longueur et de 'L' largeur. 
    
    Commence en 0 sur le dessin et suis les flèches
    
    -----<-----
    |         |
    V         A  L
    |         |
    0---->-----
         l
    '''
    for i in range(2):
        trait(l)
        tut.left(90)
        trait(L)
        tut.left(90)


### dessin diff éléments
# 
# Les dessins sont toujours commencés du points le plus en bas et le plus à gauche du dessin
#

def dessin_étage_fond(couleur:tuple)->None:
    '''
    rectangle de dimension 140x60 et de fond 'couleur'
    '''

    tut.begin_fill()
    tut.fillcolor(couleur)

    rectangle(140,60)

    tut.end_fill()



# portes
    
def porte_1()->None:
    '''
    double porte, porte automatique
    largeur : 60p
    hauteur : 50p
    '''
    tut.setheading(0)
    tut.fd(40)

    tut.begin_fill()
    tut.fillcolor((200, 200, 255))
    rectangle(60, 50)
    tut.end_fill()
    rectangle(30, 50)

def porte_2()->None:
    '''
    vielle porte en bois à haut arrondi
    largeur : 30p
    hauteur : 55p
    '''
    # positionnenment de la porte
    tut.setheading(0)
    tut.fd(20)

    tut.begin_fill()
    tut.fillcolor((230, 90, 0))

    # arc de cercle
    tut.setheading(90)
    tut.fd(40)
    tut.setheading(0)
    tut.fd(30)
    tut.setheading(90)
    tut.pendown()
    tut.circle(15, 180)
    tut.penup()
    tut.setheading(270)
    tut.fd(40)
    tut.setheading(0)

    # partie rectangulaire de la porte
    rectangle(30, 40)

    # petits rectangles dans la partie rectangulaire
    for i in range(2):
        for j in range(4):
            rectangle(15, 10)
            tut.setheading(90)
            tut.fd(10)
            tut.setheading(0)
        tut.fd(15)
        tut.setheading(90)
        tut.fd(-40)
        tut.setheading(0)

    tut.end_fill()
    
# fénetres et ballustrade

def dessin_fenètre()->None:
    '''
    fenètre carré basique (buggé mais beau donc j'ai laissé.)
    Pour débugger, il faut avancer le "tut.endfill()" de 10 lignes
    '''

    tut.begin_fill()
    tut.fillcolor((230, 230, 255))

    # encadrement
    rectangle(30,30)
    
    tut.end_fill()

    # barreaux
    tut.fd(15)
    tut.setheading(90)
    trait(30)
    tut.goto(tut.position()[0]-15, tut.position()[1]-30)
    tut.fd(15)
    tut.setheading(0)
    trait(30)
    tut.goto(tut.position()[0]-30, tut.position()[1]-15)

    

def dessin_grande_fenètre()->None:
    '''
    grande fenètre avec ballustrade
    '''
    
    # ajustement position
    tut.rt(90)
    tut.fd(20)
    tut.rt(90)

    tut.setheading(0)
    tut.begin_fill()
    tut.fillcolor((230, 230, 255))

    # fenêtre ou verre
    rectangle(30,50)

    tut.end_fill()

def rambarde()->None:
    '''
    rembarde liée à la grande fenêtre
    '''
    # barreaux
    for j in range(8):
        trait(2)
        tut.lt(90)
        trait(20)
        tut.rt(90)
        trait(2)
        tut.rt(90)
        trait(20)
        tut.lt(90)
    tut.fd(2)
    tut.rt(180)
    rectangle(34,-20)
    tut.rt(180)

    # repositionnement
    tut.lt(90)
    tut.fd(20)
    tut.rt(90)
    tut.fd(8)


# toits
def toit_1()->None:
    '''
    toit en forme de demi cercle
    '''

    tut.begin_fill()
    tut.fillcolor((51, 51, 51)) # couleur du toit

    # demi-cercle
    tut.setheading(0)
    tut.fd(140)
    tut.setheading(90)
    tut.pendown()
    tut.circle(70, 180)
    tut.penup()
    tut.setheading(0)

    tut.end_fill()



def toit_2()->None:
    '''
    toi triangulaire couleur tuile

    triangle :
    - base = 140
    - hauteur = 20
    '''
    tut.begin_fill()
    tut.fillcolor((151, 51, 1)) # couleur des tuiles

    # triangle
    tut.pendown()
    tut.goto(tut.position()[0]+70, tut.position()[1]+20)
    tut.goto(tut.position()[0]+70, tut.position()[1]-20)
    tut.goto(tut.position()[0]-140, tut.position()[1])
    tut.penup()

    tut.end_fill()
    
### differents étages et choix ds élements de chaque étage


def constr_étage(couleur:tuple)->None:
    '''
    construction d'un étage = fond + 3 fenêtres
    '''
    # fond
    dessin_étage_fond(couleur)

    # reposisionnement
    tut.setheading(0)
    tut.fd(15)
    tut.left(90)
    tut.fd(20)
    tut.setheading(0)

    # 3 fenêtres
    for i in range(3):

        # si randit = 0        --> grande fenêtre
        # si randit = 1, 2, 3  --> petite fenêtre
        # on a donc 1:3 de ratio grandes / petites fenêtres
        if rd.randint(0,3):
            dessin_fenètre()
            tut.fd(40)
                
        else:
            dessin_grande_fenètre()
            trait(-2)
            rambarde()




def r_d_c(couleur)->None:
    '''
    fond + choix de la porte
    '''
    dessin_étage_fond(couleur)

    # si randint = 0  --> porte 1 : porte automatique
    # si randint = 1  --> porte 2 : porte vieille
    # ratio 1:1
    if rd.randint(0,1):
        porte_1()
    else:
        porte_2()

def toit()->None:
    '''
    choix du toît
    '''
    # si randint = 0  --> toit 1 : demi-cercle
    # si randint = 1  --> toit 2 : triangle
    # ratio 1:1
    if rd.randint(0,1):
        toit_1()
    else:
        toit_2()


### fonction de construction de l'immeuble
def immeuble(étage:int, couleur:tuple, numero:int)->None:
    '''
    réalise les différentes étapes de construction :
    - rez-de-chausser
    - 'étage' fois étage
    - toit

    numéro = numéro de l'immeuble 
    '''

    # positionnement début de l'immeuble
    tut.goto(numero*150 - 500, -250)

    # rez-de-chausser
    r_d_c(couleur)

    # étages
    for i in range(étage):
        tut.goto(numero*150 - 500, (i+1)*60 - 250)
        constr_étage(couleur)
    
    # toit
    tut.goto(numero*150 - 500, (étage+1)*60 - 250)
    toit()
    
        
###  lancement de la construction de x immeuble

def construction(x:int, étage_min:int, étage_max:int):
    '''
    construit 'x' immeuble ayant entre 'étage_min' et 'étage_max' étage plus un toit
    '''
    for i in range(x):
        immeuble(rd.randint(étage_min, étage_max), (rd.randint(5,250), rd.randint(0,10), rd.randint(5,250)), i)



### lancement programme et valeurs injectés




construction(x, étage_min, étage_max)















for i in range(2000):
    tut.rt(5)
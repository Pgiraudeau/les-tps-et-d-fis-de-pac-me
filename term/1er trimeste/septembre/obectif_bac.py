import string
import itertools

def decale(lettre,dec):
    alph=string.ascii_uppercase
    new_index =(alph.index(lettre)+ dec)%26
    return alph[new_index]
def cesar(message, dec) :
    res = []
    for l in message: res.append(decale(l, dec))
    return "".join(res)
def Vigenere(message,passwd):
    res = []
    for lettre,decl in zip(message,
        itertools.cycle(passwd)):
        dec = string.ascii_uppercase.index(decl)
        res.append(decale(lettre , dec))
    return "".join(res)


#### correction ####
# a)

import string
import itertools 

def decale(lettre, dec):
    alph = string.ascii_uppercase
    new_index = (alph.index(lettre) + dec) % 26
    return alph[new_index]

def cesar(message, dec):
    res = []
    for l in message: 
        res.append(decale(l, dec))
    return "".join(res)

def vigenere(message, passwd):
    res = []
    for lettre, decl in zip(message, itertools.cycle(passwd)):
        dec = string.ascii_uppercase.index(decl)
        res.append(decale(lettre, dec))
    return "".join(res)


# b) ascii_uppercase -- a string containing all ASCII uppercase letters

# c)    #  class cycle(builtins.object)
        #  |  cycle(iterable) --> cycle object
        #  |  
        #  |  Return elements from the iterable until it is exhausted.
        #  |  Then repeat the sequence indefinitely.
        #  |  
        #  |  Methods defined here:

        #  class zip(object)
        #  |  zip(iter1 [,iter2 [...]]) --> zip object
        #  |  
        #  |  Return a zip object whose .__next__() method returns a tuple where
        #  |  the i-th element comes from the i-th iterable argument.  The .__next__()
        #  |  method continues until the shortest iterable in the argument sequence
        #  |  is exhausted and then it raises StopIteration.

### for lettre, decl in zip(message, itertools.cycle(passwd)):
#  
#  associe à chaque lettre du message à la lettre du passwd correspondante 
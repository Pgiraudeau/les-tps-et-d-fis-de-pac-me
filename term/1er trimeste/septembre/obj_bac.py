import string
import itertools

def decale(lettre,dec):
    alph=string.ascii_uppercase
    new_index =(alph.index(lettre)+ dec)%26
    return alph[new_index]
def cesar(message, dec) :
    res = []
    for l in message: res.append(decale(l, dec))
    return "".join(res)
def Vigenere(message,passwd):
    res = []
    for lettre,decl in zip(message,
        itertools.cycle(passwd)):
        dec = string.ascii_uppercase.index(decl)
        res.append(decale(lettre , dec))
    return "".join(res)


#### correction ####
# a)

import string
import itertools 

def decale_cor(lettre, dec):
    lettre = lettre.upper()
    alph = string.ascii_uppercase
    alphabet = []
    for l in alph:
        alphabet.append(l)
    new_index = (alphabet.index(lettre) + dec) % 26
    return alphabet[new_index]

def cesar_cor(message, dec):
    res = []
    for l in message: 
        res.append(decale_cor(l, dec))
    return "".join(res)

def vigenere_cor(message, passwd):
    res = []
    message = message.upper()
    for lettre, decl in zip(message, itertools.cycle(passwd)):
        decl = decl.upper()
        dec = string.ascii_uppercase.index(decl)
        res.append(decale_cor(lettre, dec))
    return "".join(res)


# b) ascii_uppercase -- a string containing all ASCII uppercase letters

# c)    #  class cycle(builtins.object)
        #  |  cycle(iterable) --> cycle object
        #  |  
        #  |  Return elements from the iterable until it is exhausted.
        #  |  Then repeat the sequence indefinitely.
        #  |  
        #  |  Methods defined here:

        #  class zip(object)
        #  |  zip(iter1 [,iter2 [...]]) --> zip object
        #  |  
        #  |  Return a zip object whose .__next__() method returns a tuple where
        #  |  the i-th element comes from the i-th iterable argument.  The .__next__()
        #  |  method continues until the shortest iterable in the argument sequence
        #  |  is exhausted and then it raises StopIteration.

### for lettre, decl in zip(message, itertools.cycle(passwd)):
#  
#  associe à chaque lettre du message la lettre du passwd correspondante 

# d) 

def decale_doc(lettre: str, dec: int)->str:
    '''
    renvoie la lettre correspondante après décallage en 
    fonction de la clé

    >>> decale('A', 3)
    D

    >>> decale('Y', 5)
    D
    '''
def cesar_doc(message: str, dec: int)->str:
    '''
    /!\ pas d'espaces dans le message /!\ 

    code un message en déplacent de 'dec' de a vers z
    en Majuscules

    >>> cesar('pacome', 5)
    UFHTRJ
    '''

def vigenere_doc(message: str, passwd: str)->str:
    '''
    décode un message utilisant une suite de caractère 
    (mot par exemple)

    >>> vigenere("bffyh qisfq", "ubuntu")
    hello world
    '''

# e)

# decale

assert(decale_cor('A', 2) == 'C')
assert(decale_cor('B', 25) == 'A')
assert(decale_cor('d', 26) == 'D')
assert(decale_cor('Z', 1) == 'A')
assert(decale_cor('Z', 27) == 'A')
assert(decale_cor('O', 28) == 'Q')
assert(decale_cor('A', 0) == 'A')
assert(decale_cor('B', -3) == 'Y')

# cesar

assert(cesar_cor('HelloWorld', 2) == 'jgnnqyqtnf'.upper())
assert(cesar_cor('HelloWorld', 15) == 'wtaadldgas'.upper())
assert(cesar_cor('HelloWorld', 27) == 'ifmmpxpsme'.upper())
assert(cesar_cor('HelloWorld', -1) == 'gdkknvnqkc'.upper())
assert(cesar_cor('helloworld', -1) == 'gdkknvnqkc'.upper())
assert(cesar_cor('HELLOWORLD', -1) == 'gdkknvnqkc'.upper())
assert(cesar_cor('hElLoWorLd', -1) == 'gdkknvnqkc'.upper())
assert(cesar_cor('', -1) == ''.upper())


# vigenere

assert(vigenere_cor('helloworld', 'ubuntu') == 'bffyhqisfq'.upper()) 

# 8)

def Atbash(mot: str)->str:
    '''
    code en inversant l'ordre des lettres dans l'aphabet
    '''
    new_mot = ""
    mot = mot.upper()
    alph = string.ascii_uppercase
    for l in mot:
        new_mot += alph[25 - alph.index(l)]
    return new_mot

print(Atbash('pacome'))
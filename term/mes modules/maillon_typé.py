from __future__ import annotations
from typing import Generic, TypeVar, Optional
import random

T = TypeVar("T")

class Maillon(Generic[T]):

    def __init__(self: Maillon[T], val: T) -> None:
        self._val = val
        self._suiv: Optional[Maillon[T]] = None

    def get_val(self: Maillon[T]) -> T:
        return self._val 

    def get_suiv(self: Maillon[T]) -> Optional[Maillon[T]]:
        return self._suiv

    def set_suiv(self: Maillon[T], m: Optional[Maillon[T]]) -> None:
        if m is None or (type(m._val) == type(self._val)):
            self._suiv = m
        else:
            raise TypeError  

    def __repr__(self: Maillon[T]) -> str:
        return f"[{self._val}]-->{None if self._suiv is None else self._suiv._val}"
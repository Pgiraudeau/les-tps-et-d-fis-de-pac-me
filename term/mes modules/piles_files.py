from maillon import *

class Piles:
    '''
    implementation du type abstrait pile avec des objets de type maillon
    '''

    def __init__(self):
        self._sommet = None

    def est_vide(self):
        return self._sommet is None

    def empiler(self, val):
        m = Maillon(val)
        if self.est_vide():
            self._sommet = m
        else:
            m._suiv = self._sommet
            self._sommet = m
       
    def depiler(self):
        if self.est_vide():
            raise IndexError("pile vide")
        else:
            m = self._sommet
            self._sommet = m._suiv
            return m

    def __repr__(self):
        if self.est_vide():
            return "|-"
        else:
            return f"{self._sommet}"


class Files:
    def __init__(self):
        self._first = None
        self._last = None

    def est_vide(self):
        return self._first is None

    def enfiler(self, val):
        m = Maillon(val)
        if self.est_vide():
            self._first = m
            self._last = m
            print("\n attention  __ ",m, "  :  ", self._last, " --> ", self._first, "\n")
        else:
            self._last = m
            print("\n warning  __ ",m, "  :  ", self._last, " --> ", self._first, "\n")
            
       
    def defiler(self):
        if self.est_vide():
            raise IndexError("file vide")
        else:
            m = self._first
            if m._suiv is not None:
                self._first = m._suiv
            else:
                self._first = None
                self._last = None
            return m

    def __repr__(self):
        if self.est_vide():
            return "|-"
        else:
            return f"{self._last} --> {self._first}"




##### TESTS #####
print("\n \n")

p = Files()
for k in range(5):
    
    p.enfiler(k)
    print(k, " : ", p._last)

print(p)

for k in range(5):
    print(p.defiler())

p.defiler()

import easygui as eg
import dep_editeur as de
import affichage_tableau as at
import random as rd
import import_export as imp_ex


titre = "test"
coor = (3,4)
print(type(coor[0]))

tableau = []

for ligne in range(1,22):
    ligne_creation = []
    if ligne % 2 == 0:
        for colonne in range(11):
            mur = bool(rd.randint(0,1))
            ligne_creation.append(mur)
    else:
        for colonne in range(10):
            mur = bool(rd.randint(0,1))
            ligne_creation.append(mur)
    tableau.append(ligne_creation)

nom = 'test'

imp_ex.exportation(nom, tableau)

tableau = imp_ex.importation('test.txt')

while True:
    msg = at.rendu_graphique(tableau, coor)
    dep = eg.buttonbox(msg, title=titre, choices=de.dep_possible(coor))
    tableau = de.murs(tableau, coor, dep)
    
    
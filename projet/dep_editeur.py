import easygui as eg


def déplacement(coor, mouv:str):

    if mouv == "gauche":
        new_coor = (coor[0]-1, coor[1])
    if mouv == "droite":
        new_coor = (coor[0]+1, coor[1])
    if mouv == "haut":
        new_coor = (coor[0], coor[1]-1)
    if mouv == "bas":
        new_coor = (coor[0], coor[1]+1)

    return new_coor



def dep_possible(coor):
    dep_pos = []
    if coor[0] > 0:
        dep_pos.append('gauche')

    if coor[0] < 11:
        dep_pos.append('droite')
    
    if coor[1] < 11:
        dep_pos.append('bas')

    if coor[1] > 0:
        dep_pos.append('haut')
    return dep_pos



def murs_possible(coor):
    dep_pos = []
    if coor[0] > 0:
        dep_pos.append('gauche')

    if coor[0] < 11:
        dep_pos.append('droite')
    
    if coor[1] < 11:
        dep_pos.append('bas')

    if coor[1] > 0:
        dep_pos.append('haut')
    return dep_pos


def murs(tableau, coor, direction):
    

    if direction == "gauche":
        tableau[coor[0]-1][2*coor[1]] = not(tableau[coor[0]-1][2*coor[1]])

    if direction == "droite":
        tableau[coor[0]][2*coor[1]] = not(tableau[coor[0]][2*coor[1]])

    if direction == "haut":
        tableau[coor[0]][2*coor[1]-1] = not(tableau[coor[0]][2*coor[1]-1])

    if direction == "bas":
        tableau[coor[0]][2*coor[1]-1] = not(tableau[coor[0]][2*coor[1]+1])

    return tableau
def rendu_graphique(tableau, coor):

    msg = '                  départ   \n'
    msg += '+---+---+---+---+---+   +---+---+---+---+---+\n'

    num_ligne = 1

    for ligne in tableau:

        if num_ligne % 1 == 0:
            msg += '|'

        else:
            msg += '-'

        num_col = 1

        for case in ligne:

            
            print( num_ligne % 1 == 0)
            if (num_col, num_ligne) == coor:

                if case:
                    msg += ' 0 |'
                else:
                    msg += ' 0  '

            elif num_ligne%1 == 0:

                if case:
                    msg += '   |'
                else:
                    msg += '    '

            else:
                if case:
                    msg += '---+'
                else:
                    msg += '   +'

            num_col +=1


        if num_ligne % 1 == 0:
            msg += '   |'

        else:
            msg += ''

        msg += '\n'
        num_ligne += 0.5


    msg += '+---+---+---+---+---+   +---+---+---+---+---+\n'
    msg += '                 arrivé\n'
    return msg


def importation(nom_fichier):
	
	lecteur = open(nom_fichier, 'r')

	texte = lecteur.read()
	tableau_intermediaire = texte.split('\n')

	tableau = []
	for ligne in tableau_intermediaire:
		ligne_tab = ligne.split(';')
		ligne_tab_bool = [bool(case) for case in ligne_tab]
		del ligne_tab[len(ligne_tab)-1]
		tableau.append(ligne_tab)
	del tableau[len(tableau)-1]
	return tableau

def exportation(nom, tableau):

	tableau_ecrit = []
	num_ligne = 0
	for ligne in tableau:
		ecrit = ''
		for colonne in ligne:
			ecrit = ecrit + str(colonne) + ';'

		tableau_ecrit.append(ecrit)
		num_ligne += 1

	with open(nom + '.txt', 'w') as fic_laby:
		for ligne in tableau_ecrit:
	
			fic_laby.write(ligne + '\n')

		return None

importation('test.txt')